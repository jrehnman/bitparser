
module BitString where

import Data.Bits (Bits, shiftL, shiftR, (.|.), (.&.))
import qualified Data.ByteString as B
import Data.Ratio ((%))
import Prelude hiding (take, drop, length, null, concat)
import qualified Prelude
import Data.Word (Word8)

{-
     byte 2          byte 1          byte 0
+---------------+---------------+---------------+
|7 6 5 4 3 2 1 0|7 6 5 4 3 2 1 0|7 6 5 4 3 2 1 0| bytestr
+---^-----------+---------------+-----^---------+
   _|                                 |_
   loffset=2                          roffset=5

    +-----------+---------------+-----+
    |. . F E D C|B A 9 8 7 5 4 3|2 1 0|           bitstring
    +-----------+---------------+-----+
-}

data BitString = BitString
    { bytestr   :: B.ByteString
    , loffset   :: Int
    , roffset   :: Int
    } deriving Show

instance Eq BitString where
    a == b = (rUnpack a) == (rUnpack b)

empty :: BitString
empty = pack' []

take :: Int -> BitString -> BitString
take n bs = 
    let neededbits      = n + (roffset bs)
        bytes           = B.take (minimumBytesFor neededbits) (bytestr bs)

        actualbytes     = B.length bytes
        availablebits   = (actualbytes * 8) - (roffset bs)
        actualbits      = min availablebits n
    in BitString 
        { bytestr = bytes
        , roffset = roffset bs 
        , loffset = availablebits - actualbits
        }

drop :: Int -> BitString -> BitString
drop n bs = 
    let nbytes = (roffset bs + n) `div` 8
        bytes = B.drop nbytes (bytestr bs)
    in if B.null bytes then empty else BitString
        { bytestr = bytes
        , roffset = (roffset bs + n) `mod` 8
        , loffset = loffset bs
        }

length :: BitString -> Int
length bs = (B.length (bytestr bs) * 8) - (roffset bs) - (loffset bs)

null :: BitString -> Bool
null bs = 
    let sample = B.take 2 (bytestr bs)
    in (B.length sample) * 8 <= (loffset bs) + (roffset bs)

pack :: B.ByteString -> BitString
pack bs = BitString 
    { bytestr = bs
    , roffset = 0
    , loffset = 0
    }

pack' :: [Word8] -> BitString
pack' = pack . B.pack

rJustify :: BitString -> BitString
rJustify bs 
    | null bs           = pack' []
    | roffset bs == 0   = bs
    | otherwise         =
        let bytes = bytestr bs
            nbits = length bs
            nbytes = minimumBytesFor nbits
            trimbits = roffset bs
            headBits = B.map (`shiftR` trimbits) bytes
            tailBits = B.map (`shiftL` (8 - trimbits)) (B.tail bytes)
            bytes' = B.zipWith (.|.) headBits (tailBits `B.snoc` 0x00)
        in if null bs then empty else BitString
            { bytestr = B.pack (Prelude.take nbytes bytes')
            , roffset = 0
            , loffset = (nbytes * 8) - nbits
            }

rUnpack :: BitString -> B.ByteString
rUnpack bs = 
    let bs' = rJustify bs
        bytes = bytestr bs'
        trimmask = 0xff `shiftR` (loffset bs')
    in if null bs' 
        then B.empty
        else (B.init bytes) `B.snoc` (B.last bytes .&. trimmask)

reinterpret :: Bits i => B.ByteString -> i
reinterpret bs = 
    let bytes = map fromIntegral (B.unpack bs)
    in if B.null bs
        then 0
        else foldl1 (concat 8) bytes

reinterpret' :: Bits i => BitString -> i
reinterpret' = reinterpret . rUnpack

reinterpretTake :: Bits i => Int -> BitString -> i
reinterpretTake n bs = reinterpret' (take n bs)

{- 
 - Utilities
 -}
concat :: Bits i => Int -> i -> i -> i
concat width msbits lsbits = (msbits `shiftL` width) .|. lsbits

-- Number of bytes necessary to fit the requested number of bits
minimumBytesFor :: Int -> Int
minimumBytesFor bits = ceiling (bits % 8)

