module BitParser where

import Control.Monad
import Data.Bits (Bits)
import Prelude hiding (any)
import Text.Printf (printf, PrintfArg)

import BitString (BitString)
import qualified BitString as BS


-- UnconsumedStream -> StreamPosition -> (Either x ErrorMsg, ConsumedStream, ConsumedSize)
newtype Parser s x = P { runParser :: s -> Int -> (Either x String, s, Int) }

instance Monad (Parser s) where
    return x = P $ \s _ -> (Left x, s, 0)
    fail err = P $ \s _ -> (Right err, s, 0)
    ma >>= f = P $ \s n ->
        let (r, s', m) = runParser ma s n
        in case r of
            Left a -> let (q, s'', o) = runParser (f a) s' (n + m) in (q, s'', o+m)
            Right err -> (Right err, s', m)

instance MonadPlus (Parser s) where
    mzero = P $ \s n -> (Right "ZeroMonad", s, n)
    mplus = (<|>)

type BitParser = Parser BitString

parse :: Parser s x -> s -> Either x String
parse p s = let (result, _, _) = runParser p s 0 in result

(<|>) :: Parser s a -> Parser s a -> Parser s a
p <|> q = P $ \s n ->
    let a@(r, _, _) = runParser p s n
    in case r of
        Left _ -> a
        Right _ -> runParser q s n

(?) :: BitParser x -> String -> BitParser x
p ? msg = P $ \s n ->
    let (y, s', m) = runParser p s n
    in case y of 
        Left _  -> (y, s', m)
        Right _ -> (Right msg, s', m)

bits :: Bits b => (b, Int) -> BitParser b
bits (b, w) = do
    b' <- anyBits w
    if (b == b')
        then return b
        else fail $ "Unexpected bits"

any :: Int -> BitParser BitString
any sz = P $ \s n ->
    let x = BS.take sz s
        s' = BS.drop sz s
    in (Left x, s', sz)

anyBits :: Bits b => Int -> BitParser b
anyBits w = any w >>= return . BS.reinterpret'

tell :: BitParser Int
tell = P $ \s n -> (Left n, s, 0)

parserSize :: BitParser x -> BitParser (x, Int)
parserSize p = P $ \s n -> 
    let (r, s', m) = runParser p s n
    in case r of
        Left b  -> (Left (b, m), s', m)
        Right e -> (Right e, s', m)

